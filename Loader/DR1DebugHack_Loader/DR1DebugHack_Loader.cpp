// DR1DebugHack_Loader.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include "injector.h"


int DoInject()
{
	printf("Injecting...\n");
	CInjector* cInjector = new CInjector();

	if (!cInjector)
	{
		printf("Failed to create injector class??\n");
		system("pause");
		return 2;
	}

	bool bInjectionSuccessful = cInjector->InjectDLL();
	if (!bInjectionSuccessful)
	{
		//Something went wrong, pause to show the error.
		system("pause");
		return 1; //Failure
	}
	system("pause");
	return 0; //Success! \o/
}

int DebugPriv(void){
	HANDLE hToken;
	LUID Value;
	TOKEN_PRIVILEGES tp;
	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		return(GetLastError());
	if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &Value))
		return(GetLastError());
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = Value;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	if (!AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(tp), NULL, NULL))
		return(GetLastError());
	CloseHandle(hToken);
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	SetConsoleTitle(L"Jill's DR1 Debug Hack: Loader");
	if (DebugPriv())
	{
		printf("Debug privileges granted...\n");
	}
	else
	{
		printf("Failed to get debug privileges!\n");
		system("pause");
		return 3;
	}
	return DoInject();
}


//////////////////////////////////////////////////////////////////////////
//Most of this stuff is borrowed from SaEeD's DLL Injector
//Please contact me if you own the borrowed code and wish for it to be removed.
//https://github.com/saeedirha/DLL-Injector
//////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <ctype.h>
#include <Windows.h>
#include <tlhelp32.h>
#include "injector.h"
#include <comdef.h>

int CInjector::getProcID( void )
{
	const char* p_name = "dr1_us.exe";
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 structprocsnapshot = { 0 };

	structprocsnapshot.dwSize = sizeof(PROCESSENTRY32);

	if (snapshot == INVALID_HANDLE_VALUE)return 0;
	if (Process32First(snapshot, &structprocsnapshot) == FALSE)return 0;

	while (Process32Next(snapshot, &structprocsnapshot))
	{
		_bstr_t exe(structprocsnapshot.szExeFile);
		if (!strcmp(exe, p_name))
		{
			CloseHandle(snapshot);
			printf("Process name is: %s, Process ID: %i\n", p_name, structprocsnapshot.th32ProcessID);
			return structprocsnapshot.th32ProcessID;
		}
	}
	CloseHandle(snapshot);
	printf("Failed to find dr1_us.exe. Please launch the game and press any key to try again.\n");
	system("pause");
	return 0;

}

bool CInjector::InjectDLL( void )
{
	const char* DLL_Path = "./DR1DebugHack_Lib.dll";
	long dll_size = strlen(DLL_Path);
	int procID = 0;
	while (procID == 0)
	{
		procID = getProcID();
	}
	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, procID);

	if (hProc == NULL)
	{
		printf("[!]Fail to dr1_us.exe!\n");
		return false;
	}
	printf("[+]Opening Target Process...\n");

	LPVOID MyAlloc = VirtualAllocEx(hProc, NULL, dll_size, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (MyAlloc == NULL)
	{
		printf("[!]Failed to allocate memory in Target Process.\n");
		return false;
	}

	printf("[+]Allocating memory in Target Process.\n");
	int IsWriteOK = WriteProcessMemory(hProc, MyAlloc, DLL_Path, dll_size, 0);
	if (IsWriteOK == 0)
	{
		printf("[!]Fail to write in Target Process memory.\n");
		return false;
	}
	printf("[+]Creating Remote Thread in Target Process\n");

	DWORD dWord;
	LPTHREAD_START_ROUTINE addrLoadLibrary = (LPTHREAD_START_ROUTINE)GetProcAddress(LoadLibrary(L"kernel32.	dll"), "LoadLibraryA");
	HANDLE ThreadReturn = CreateRemoteThread(hProc, NULL, 0, addrLoadLibrary, MyAlloc, 0, &dWord);
	if (ThreadReturn == NULL)
	{
		printf("[!]Fail to create Remote Thread\n");
		return false;
	}

	if ((hProc != NULL) && (MyAlloc != NULL) && (IsWriteOK != ERROR_INVALID_HANDLE) && (ThreadReturn != NULL))
	{
		printf("Debug hack successfully injected!\n");
		return true;
	}

	printf("Unspecified error!\n");
	return false;
}

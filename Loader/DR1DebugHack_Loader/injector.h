#ifndef _INJECTOR_H
#define _INJECTOR_H
//////////////////////////////////////////////////////////////////////////
//Most of this stuff is borrowed from SaEeD's DLL Injector
//Please contact me if you own the borrowed code and wish for it to be removed.
//https://github.com/saeedirha/DLL-Injector
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//Class: CInjector
//Purpose: Main injector class
//Notes: Borrowed from SaEeD's DLL Injector.
//https://github.com/saeedirha/DLL-Injector
//////////////////////////////////////////////////////////////////////////
class CInjector
{

	// Get Process ID by its name. Borrowed from SaEeD's DLL Injector, modified to be hardcoded to dr1_us.
	int getProcID(void);

public:
	//InjectDLL: Injects the DLL. Borrowed from SaEeD's DLL Injector, modified to be hardcoded to DR1DebugHack.
	bool InjectDLL(void);
};
#endif //!_INJECTOR_H

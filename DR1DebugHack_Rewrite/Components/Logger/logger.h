#ifndef _LOGGER_H
#define _LOGGER_H
#include <windows.h>
#include <iostream>

//////////////////////////////////////////////////////////////////////////
//Class: CLogger
//Purpose: Console logging
//////////////////////////////////////////////////////////////////////////

class CLogger
{
private:
	HANDLE stdHandle;
	int hConsole;
	FILE* fp;

public:

	bool Logger_Init();
};
#endif

#include "logger.h"
#include <iostream>
#include <windows.h>
#include <io.h>
#include <fcntl.h>

bool CLogger::Logger_Init()
{
	BOOL bConsole = AllocConsole();

	if (bConsole)
	{
		char pszWindowTitle[MAX_PATH];
		//GetModuleFileNameExA(hDangan, NULL, pszExecutableName, MAX_PATH);

		sprintf_s(pszWindowTitle, "Jill's DR Debug Hack: Console");
		wchar_t wWindowTitle[MAX_PATH];
		mbstowcs(wWindowTitle, pszWindowTitle, strlen(pszWindowTitle) + 1);//Plus null

		SetConsoleTitle(wWindowTitle);

		//Route output to console
		stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		hConsole = _open_osfhandle((long)stdHandle, _O_TEXT);
		fp = _fdopen(hConsole, "w");

		freopen_s(&fp, "CONOUT$", "w", stdout);

	}
	return bConsole;
}
